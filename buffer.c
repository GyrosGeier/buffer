#define _GNU_SOURCE 1

#include <pthread.h>

#include <sys/types.h>
#include <sys/mman.h>

#include <unistd.h>
#include <errno.h>

#include <stdbool.h>
#include <stdio.h>

struct global
{
	/* file descriptors */
	int in, out;

	/* ring buffer for data */
	unsigned char *buffer;
	size_t size, readp, writep;

	pthread_mutex_t mutex;
	pthread_cond_t read_cond, write_cond;

	bool read_active, write_active, done;
};

static void *statistics_thread(void *data)
{
	struct global *const g = data;

	pthread_mutex_lock(&g->mutex);
	do
	{
		size_t const readp = g->readp;
		size_t const writep = g->writep;
		pthread_mutex_unlock(&g->mutex);
		fprintf(stderr, "\r%20zi %20zi\e[0K", readp, writep);
		usleep(10000);
		pthread_mutex_lock(&g->mutex);
	}
	while(!g->done || g->readp != g->writep);
	pthread_mutex_unlock(&g->mutex);

	return NULL;
}

static void *write_thread(void *data)
{
	size_t const max_write = 0x1000000ull;		// 16 MiB

	struct global *const g = data;

	unsigned char *const buffer = g->buffer;
	size_t const size = g->size;

	pthread_mutex_lock(&g->mutex);
	g->write_active = true;

	for(;;)
	{
		size_t const readp = g->readp;
		size_t const writep = g->writep;
		bool const done = g->done;

		bool const is_wrapped = (writep < readp);
		size_t const ravail =
				is_wrapped
				? size - readp
				: writep - readp;

		if(!ravail)
		{
			if(done)
				break;

			if(!g->read_active)
				pthread_cond_signal(&g->read_cond);
			g->write_active = false;
			pthread_cond_wait(&g->write_cond, &g->mutex);
			g->write_active = true;
			continue;
		}

		pthread_mutex_unlock(&g->mutex);

		size_t const write_size =
				(ravail < max_write)
				? ravail
				: max_write;

		ssize_t const num_written = write(g->out, buffer + readp, write_size);

		pthread_mutex_lock(&g->mutex);

		if(num_written <= 0)
			break;
		else if(g->readp + num_written >= g->size)
			g->readp = 0;
		else
			g->readp += num_written;

		if(!g->read_active)
			pthread_cond_signal(&g->read_cond);
	}

	pthread_mutex_unlock(&g->mutex);

	return NULL;
}

int main(int argc, char **argv)
{
	(void)argc;
	(void)argv;

	size_t const size = 0x200000000ull;	// 8 GiB

	unsigned char *const buffer = mmap(
			NULL,
			size,
			PROT_READ|PROT_WRITE,
			MAP_PRIVATE|MAP_ANONYMOUS|MAP_HUGETLB,
			-1,
			0);

	struct global g =
	{
		.in = STDIN_FILENO,
		.out = STDOUT_FILENO,
		.buffer = buffer,
		.size = size,
		.readp = 0,
		.writep = 0,
		.mutex = PTHREAD_MUTEX_INITIALIZER,
		.read_cond = PTHREAD_COND_INITIALIZER,
		.write_cond = PTHREAD_COND_INITIALIZER,
		.read_active = false,
		.write_active = false,
		.done = false
	};

	pthread_mutex_lock(&g.mutex);
	g.read_active = true;

	/*
	pthread_t statistics;
	if(pthread_create(&statistics, NULL, &statistics_thread, &g) != 0)
		return 1;
	 */

	pthread_t writer;
	if(pthread_create(&writer, NULL, &write_thread, &g) != 0)
		return 1;

	for(;;)
	{
		size_t const readp = g.readp;
		size_t const writep = g.writep;
		bool const done = g.done;

		/* buffer management: write side of ring buffer */
		bool const is_wrapped = (writep < readp);
		bool const can_wrap = (readp > 0);

		size_t const wavail =
				is_wrapped
				? readp - writep - 1
				: (can_wrap)
				  ? size - writep
				  : size - writep - 1;

		if(done)
			break;

		if(!wavail)
		{
			if(!g.write_active)
				pthread_cond_signal(&g.write_cond);
			g.read_active = false;
			pthread_cond_wait(&g.read_cond, &g.mutex);
			g.read_active = true;
			continue;
		}

		pthread_mutex_unlock(&g.mutex);

		ssize_t const num_read = read(g.in, buffer + writep, wavail);

		pthread_mutex_lock(&g.mutex);

		if(num_read < 0)
			break;
		else if(num_read == 0)
			g.done = true;
		else if(g.writep + num_read >= size)
			g.writep = 0;
		else
			g.writep += num_read;

		if(!g.write_active)
			pthread_cond_signal(&g.write_cond);
	}

	pthread_mutex_unlock(&g.mutex);

	void *ret;
	pthread_join(writer, &ret);

	/*
	pthread_join(statistics, &ret);
	 */

	return 0;
}
