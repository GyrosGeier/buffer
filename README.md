# Simple buffering tool

This can be inserted into pipelines to give some buffer space. Currently
this is hardcoded to 8 GiB in hugepages (so unlikely to be swapped out),
since that is what my application needs.

This can probably be refined for more uses.
